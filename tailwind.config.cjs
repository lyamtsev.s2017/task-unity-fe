/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './**/*.vue'],
  theme: {
    container: {
      center: true,
    },
    extend: {
      fontFamily: {
        sans: ['Inter', 'sans-serif'],
      },
      borderRadius: {
        10: '10px',
      },
      colors: {
        blue: {
          200: '#cdcfd1',
          300: '#646870',
          400: '#8d939a',
          500: '#1a44d9',
          900: '#26292d',
        },
        red: {
          500: '#d0340c',
        },
      },
    },
  },
  plugins: [],
}
