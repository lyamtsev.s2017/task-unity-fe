# Vue 3 + TypeScript + Vite

## Recommended setup
- [Volta.sh](https://volta.sh/)

## Project set up

```bash
# Install dependencies
$ yarn install 

# Compiles and hot-reloads for development
$ yarn dev

# Compiles and minifies for production
$ yarn build
```

## Prepare local environment

### Create .env file
Copy `.env.example` to `.env` (you have to create it) and set up your environment variables if needed

### Prepare git hooks
```bash
$ git config --unset-all core.hooksPath && npx husky install
```


