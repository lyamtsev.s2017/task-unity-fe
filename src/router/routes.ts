import { RouteRecordRaw } from 'vue-router'
import { routeNames } from './constants.ts'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: routeNames.home,
    component: () => import('../views/HomePage/HomePage.vue'),
  },
  {
    path: '/login',
    name: routeNames.login,
    component: () => import('../views/LoginPage/LoginPage.vue'),
  },
  {
    path: '/register',
    name: routeNames.register,
    component: () => import('../views/RegisterPage/RegisterPage.vue'),
  },
]

export { routes }
