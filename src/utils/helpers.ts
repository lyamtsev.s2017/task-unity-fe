// Capitalize the first letter of sentences
export const upperFirst = (str: string) => {
  return str ? str.charAt(0).toUpperCase() + str.slice(1) : ''
}
