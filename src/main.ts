import { createApp } from 'vue'
import App from './App.vue'
import { router } from './router/router.ts'
import { pinia } from './store/pinia.ts'
import './assets/css/main.scss'

const app = createApp(App)

app.use(router)
app.use(pinia)

app.mount('#app')
