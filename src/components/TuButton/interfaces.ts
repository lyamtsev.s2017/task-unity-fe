export type TuButtonVariant =
  | 'primary'
  | 'secondary'
  | 'secondary-icon'
  | 'tertiary'
  | 'tertiary-grey'
