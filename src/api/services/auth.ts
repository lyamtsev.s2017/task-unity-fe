import { ApiInstance } from '../interfaces.ts'

interface SignUpDTO {
  email: string
  username: string
  password: string
}

interface SignInDTO {
  email: string
  password: string
}

export default (api: ApiInstance) => ({
  signUp: (dto: SignUpDTO) => api.post('/auth/sign-up', dto),
  signIn: (dto: SignInDTO) => api.post('/auth/sign-in', dto),
  refresh: () => api.post('/auth/refresh'),
  logoutFromAllDevices: () => api.post('/auth/logout-all-devices'),
})
