import { apiRoot } from './instances.ts'
import auth from './services/auth.ts'

const api = {
  auth: auth(apiRoot),
}

export { api }
