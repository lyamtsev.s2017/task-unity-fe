import { AxiosInstance } from 'axios'

export type ApiInstance = AxiosInstance
