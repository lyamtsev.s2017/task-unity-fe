import axios from 'axios'

const apiRoot = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  withCredentials: true,
})

export { apiRoot }
